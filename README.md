xDec
====

xDec is a PHP base web application development framework. It is inspired by Modal-View-Controller architecture and focuses on singleton and object registery. Singleton object registery makes, easier to develop, debug and deploy web applications. It evolved from compiling scripts required for a simple website. It may have some bugs or security issues.

Installation Guidelines
-----------------------

1. Upload all files to public html directory
2. Edit config.inc.php for your application specific settings
3. And thats all

Usage Guidelines
----------------

Creating a page
 * To create a page, a php class is constructed and placed in ~/contents directory
    File naming convention for pages - \<your_page\>.page.php
    
 * Now bind the page to a url
    In bind.php add an array element to $binds array and map it with the regular expression for binding url

        eg:-
      
        '/^\/?$/' => array("folder" => ".", "file" => "\<your_page\>", "cache" => "true");
      
   This will create a page mapped with url <your_domain>/ and will cache the content of your page.
   
